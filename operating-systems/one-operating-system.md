---
description: An operating system based on GNU/Genshin.
---

# 1 One Operating System

### History

The GNU/Genshin Project was formed on July,28,2015, they released the first alpha version at the end of this month.&#x20;

In 2016, FlashTeens dumplicated GNU/Genshin 's source code , and changed its name to "projectOne".

&#x20;After that, ProjectOne is separated from GNU/Genshin. In 2018, projectOne changed its name to" The One Operating System", since it was the first OS that designed for personal use.&#x20;

Unlike GNU/Genshin, The One Operating System is more inclined to use in general individuals.

### Features

Although One Operating System is based on GNU/Genshin, with the development of the operating system, the differences between One Operating System and GNU/Genshin are gradually obvious.&#x20;

GNU/Genshin doesn't come with a graphical environment by default, since it was designed for embedded devices. Because the operating system of the embedded device must be quickly booted after the device is powered on, so under this condition, the graphic user interface will become useless.

In order to better meet the user's experience, One OS uses GNOME as its default graphic user interface. At the same time, we also developed a series of software that meets the basic use of users.

### Kernel

One Operating System use a standard FreeBSD kernel in order to compitable other hardwares like USB camaras,USB drives, printers,etc.&#x20;

In additional, advanced users can also compile their own kernel.

### Versions

One Operating System and GNU/Genshin are developed simultaneously, and follow the GNU/Genshin 's version of the distribution strategy.

### Lifecycle\&Security

One Operating System usually releases major versions once a year, and minor versions are released in the form of updates. The normal version is supported for 3 months, the long-term support version is 10 years, but the Primogem Updates version is 50 years.

Every application of One Operating System runs in the native library, but also provides a sandbox, which virtualizes a complete GNU/Genshin system instead of One Operating System , and programs inside the sandbox cannot access files outside the sandbox. All datas will be automatically destroyed when you close the sandbox.
