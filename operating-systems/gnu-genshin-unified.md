---
description: An BSD-Based operating system designed for embedded devices.
---

# 🌟 GNU/Genshin Unified

## Overview

GNU/Genshin Unified is a FreeBSD-based, open-source operating system for everyone. We make it to just enough for your devices, support your device released more than 20 years ago. Of course, it's entirely free!



{% hint style="info" %}
**Note:** You can find full infomations about GNU/Genshin here:[http://gnu-genshin.wikidot.com/wiki:gnu-genshin](http://gnu-genshin.wikidot.com/wiki:gnu-genshin)
{% endhint %}

<figure><img src="../.gitbook/assets/gnu-genshin.svg" alt=""><figcaption></figcaption></figure>



## Whatever your device is, GNU/Genshin Unified will make it better. <a href="#toc1" id="toc1"></a>

<table data-card-size="large" data-view="cards"><thead><tr><th align="center"></th><th></th><th></th></tr></thead><tbody><tr><td align="center"><strong></strong><span data-gb-custom-inline data-tag="emoji" data-code="2699">⚙</span><strong>Multi-boot Support</strong></td><td>GNU/Genshin Unified supports many boot methods : From the most popular EFI and Legacy BIOS, to Das U-Boot,CoreBoot and even our self developed bootloader — GEUB ! Boot GNU/Genshin Unified on every device is easier than other OS.</td><td></td></tr><tr><td align="center"><strong></strong><span data-gb-custom-inline data-tag="emoji" data-code="1f5a5">🖥</span><strong>Cross-platform Support</strong></td><td>No matter your device's architecture is, you will always find the best for you devices. From classic AMD64/x86, to aarch64, armhf, Itanium, MIPS and RISC-V. We bring the universal experience to our users.</td><td></td></tr><tr><td align="center"><strong></strong><span data-gb-custom-inline data-tag="emoji" data-code="1f5e3">🗣</span><strong>Community</strong></td><td>You can build or looking for a customed OS that published by our great community. Buliding your own system has never easier than before.</td><td></td></tr><tr><td align="center"><strong></strong><span data-gb-custom-inline data-tag="emoji" data-code="1f550">🕐</span><strong>Longevity</strong></td><td>GNU/Genshin Unified extends the functionality and lifespan of embedded devices from more than 20 different manufacturers thanks to our open-source community of contributors from all around the world.</td><td></td></tr><tr><td align="center"><strong>Security</strong></td><td>Your data, your rules. Along with monthly security updates to every supported device, we enhance existing privacy touchpoints around the OS and keep you informed of how the system shares your data.</td><td></td></tr><tr><td align="center"><strong>We are open-source.</strong></td><td>Open your eyes, see the worlds. Our repository is available at GitHub .</td><td></td></tr><tr><td align="center"><strong>Power to you.</strong></td><td><p>Our open-source apps are here to help you get through the day. </p><p>Want to do more with your device? </p><p>Power users will enjoy Unix command-line utilities.</p></td><td></td></tr></tbody></table>



### History

The GNU/Genshin Project was formed on July,28,2015, they released the first alpha version at the end of this month. Unlike their recent releases, their first release is Linux-based, and they use XFCE Desktop as its default graphical user interface.There is a hidden file at the root directory, but nothing in there. A beta version was released in February 2016, at the same time it was announced that the project had entered its first stable milestone.The first public beta version was released in April of the same year, in which a new FreeBSD-based kernel has been adopted, but since it does not come with a desktop environment by default, there are not many testers. But this release also lays the groundwork for future iterations.

### Features

GNU/Genshin Unified comes with a package manager called [GI Package Manager](http://gnu-genshin.wikidot.com/wiki:package-manager).Unlike regular package managers, it does not require sudo to execute itself. Its built-in software repositories includes 16 million software from multiple platforms, in addition, users can also add software repositories by themselves.

#### File System

Its default file system, called FTFS(FlashTeens File System).In order to maintain compatibility with UNIX, FTFS still adopts the root directory structure and stores all applications in the "/Apps " directory. FTFS supports encryption and supports case-sensitive encryption.

It is the top layer of the Genshin Kernel and is mainly responsible for interacting with the user, such as a terminal or desktop environment.

GNU/Genshin Unified usually releases major versions once a year, and minor versions are released in the form of updates. The normal version is supported for 3 months, the long-term support version is 10 years, but the Primogem Updates version is 50 years.
