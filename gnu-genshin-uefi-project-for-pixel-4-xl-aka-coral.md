---
description: A project helps porting UEFI Firmware to Pixel 4 XL Phone.
---

# 📶 GNU/Genshin UEFI Project for Pixel 4 XL(aka coral)

{% hint style="warning" %}
This project is under development. If you use our UEFi firmware,your phone may fail to boot or meet other issues.
{% endhint %}

{% hint style="danger" %}
**Warning:** If you re-enable UFS support(By adding UFSDxe part in DXE\_Spec.inc), your Pixel's internal UFS flash storage will be wiped clean.

We will not be responsible for the data loss caused by this.\
&#x20;  _**YOU HAVE BEEN WARNED.**_
{% endhint %}

{% hint style="info" %}
This Repository is forked from [edk2-porting/MU-sm8150pkg](https://github.com/edk2-porting/MU-sm8150pkg)
{% endhint %}

<figure><img src=".gitbook/assets/drawing-2.png" alt=""><figcaption></figcaption></figure>

Links:

{% embed url="https://github.com/Android-Piepaint/UEFI-For-Coral" %}

### Build guide(If you use orginal repository from edk2-porting)

{% hint style="success" %}
**Did you know ?** You can use this document as a reference if you have a Qualcomm Snapdragon® 855 (SM8150) device !
{% endhint %}

{% hint style="info" %}
First of all, clone our repository on your Linux device : `git clone` [`https://github.com/Android-Piepaint/UEFI-For-Coral.git`](https://github.com/Android-Piepaint/UEFI-For-Coral.git)``
{% endhint %}



1. copy nubia-tp1803 to google-coral. (Brand-codename).
2. Remove all files under google-coral/CustomizedBinaries & google-coral/PatchedBinaries.
3. Extract files from your device's xbl.img file using these commands:

```bash
dd if=/dev/block/by-name/xbl_a  of=/sdcard/xbl.img
```

\--copy or move it to your computer.

5\. Now, move the xbl.img to your Windows® PC, download the latest version of 7-Zip.

6\. In 7-Zip, find your xbl.img ,right click -> 7zip- > extract -> #:e -> 4.gz - you will see all efi files in 4.gz.

<figure><img src=".gitbook/assets/1.png" alt=""><figcaption></figcaption></figure>

<figure><img src=".gitbook/assets/2 (1).png" alt=""><figcaption></figcaption></figure>

<figure><img src=".gitbook/assets/3 (1).png" alt=""><figcaption></figcaption></figure>

<figure><img src=".gitbook/assets/4.png" alt=""><figcaption></figcaption></figure>

<figure><img src=".gitbook/assets/5.png" alt=""><figcaption></figcaption></figure>

<figure><img src=".gitbook/assets/6.png" alt=""><figcaption></figcaption></figure>

<figure><img src=".gitbook/assets/7.png" alt=""><figcaption></figcaption></figure>

<figure><img src=".gitbook/assets/8.png" alt=""><figcaption></figcaption></figure>

<figure><img src=".gitbook/assets/9_final.png" alt=""><figcaption><p>You should get these.</p></figcaption></figure>

7\. Put them under CustomizedBinaries.

8\. Enable MLVM in Defines.dsc.inc.

<figure><img src=".gitbook/assets/Screenshot 2022-11-09 at 6.12.34 PM.png" alt=""><figcaption><p>This file.</p></figcaption></figure>

<figure><img src=".gitbook/assets/Screenshot 2022-11-09 at 6.12.47 PM.png" alt=""><figcaption><p>You can use Nano text editor under Linux.</p></figcaption></figure>

9\. Edit resolution in PcdFixedAtBuild.dsc.inc.

<figure><img src=".gitbook/assets/Screenshot 2022-11-09 at 6.16.36 PM.png" alt=""><figcaption></figcaption></figure>

<figure><img src=".gitbook/assets/Screenshot 2022-11-09 at 6.16.42 PM.png" alt=""><figcaption></figcaption></figure>

10\. Remove UFSDxe part in DXE\_Spec.inc.

{% hint style="danger" %}
**Warning:** Take look at these pictures carefully ! If you made a mistake is this step, your Pixel's internal UFS flash storage will be wiped due to Google's bootloader feature.

<mark style="color:red;">**Once you made this mistake, there is no turning back.**</mark>

Again: We will not be responsible for the data loss caused by this**.**\
&#x20;  _**YOU HAVE BEEN WARNED.**_
{% endhint %}

{% hint style="success" %}
**Don't panic !** It is safe to just run UEFI image if you don't boot Windows® on it.\
![](<.gitbook/assets/Screenshot 2022-11-06 at 11.59.43 AM.png>)
{% endhint %}

<figure><img src=".gitbook/assets/Screenshot 2022-11-09 at 6.12.34 PM.png" alt=""><figcaption><p>This one.</p></figcaption></figure>

<figure><img src=".gitbook/assets/Screenshot 2022-11-09 at 6.26.29 PM.png" alt=""><figcaption><p>Before(You should remove the highlighted part)</p></figcaption></figure>

<figure><img src=".gitbook/assets/Screenshot 2022-11-09 at 6.26.32 PM.png" alt=""><figcaption><p>After</p></figcaption></figure>

11\. Patch your device's dxe and put them under PatchedBinaries.

{% hint style="info" %}
**How to patch :**

You can use IDA to patch it.&#x20;

*   To know where needs patch, you need to refer other devices:

    ```
    hexdump -C  a.efi > a.txt
    hexdump -C b.efi > b.txt
    diff a.txt b.txt
    ```
* a.efi is original binary, b.efi is a patched binary.
* The diff will tell you where and how to patch.
* Find the same function in your device's dxe.
* Patch it with IDA.
{% endhint %}

12\. Replace tp1083.dtb with coral.dtb. You can find your device's dtb in `/sys/firmware/fdt`.(Both pacthed file and DTB file are missing. I'll upload them later.)

13\. Time to bulid, enter these command:

```bash
./build_uefi.sh -d google-coral -s 6
```

**This is order.(此乃天道。）**

14\. Finally, boot your UEFI image !&#x20;

```bash
fastboot boot Build/google-coral/google-coral_xG.img
```

### Build(from our repository)



Quick notes for building:

* Use Ubuntu 20.04 x64 (or use docker-compose under other distros)
* Generate ACPI tables with IASL
* Follow this quick draft

1. Setup Base environment

```
./setup_env.sh
pip install --upgrade -r pip-requirements.txt
```

Alternatively, use docker if you don't have Ubuntu 20.04 environment:

<pre class="language-bash"><code class="lang-bash"><strong>docker build -t mu:v1 .
</strong>docker run -it mu:v1 -v ./:/build/
</code></pre>

Then finish the following process in docker environment.



2\. Activate Workspace

```bash
python3 -m venv SurfaceDuo
source SurfaceDuo/bin/activate
```

3\. Setup Mu environment

```
./setup_uefi.sh
```



4\. Stamp build

```
python3 ./Platforms/SurfaceDuo1Pkg/StampBuild.py
```

or

```
./build_releaseinfo.ps1
```



5\. Build UEFI

> Usage: build\_uefi.sh -d -s \
> Optional: -m -r -u -b&#x20;

```
./build_uefi.sh -d <target-name> -s <target-ram-size> [-m <Model> -r <RetailModel> -u <RetailSku> -b <BoardModel>]
```

* Ram size should be 6, 8, or 12.
* You will find Build/\<target-name>/\<target-name>\_\<target-ram-size>G.img if it builds successfully.

### Target list(from edk2-porting orginal repository,well, most.)

| Device             | Target name         | DSDT Support | Maintainers                                             |
| ------------------ | ------------------- | ------------ | ------------------------------------------------------- |
| ASUS ROG2          | asus-I001DC         | ✅            | [Ww](https://github.com/Idonotkno)                      |
| LG G8              | lg-alphaplus        | ✅            | [Molly Sophia](https://github.com/MollySophia)          |
| LG G8S             | lg-betalm           | ✅            | [J0SH1X](https://github.com/J0SH1X)                     |
| LG G8X             | lg-mh2lm            | ✅            | [Molly Sophia](https://github.com/MollySophia)          |
| LG V50             | lg-flashlmdd        | ✅            | [AKA](https://github.com/AKAsaliza)                     |
| LG V50S            | lg-mh2lm5g          | ✅            | [AKA](https://github.com/AKAsaliza)                     |
| Nubia Mini 5G      | nubia-tp1803        | ✅            | [Alula](https://github.com/alula)                       |
| OnePlus 7 Pro      | oneplus-guacamole   | ✅            | [Waseem Alkurdi](https://github.com/WaseemAlkurdi)      |
| OnePlus 7T Pro     | oneplus-hotdog      | ✅            | [sunflower2333](https://github.com/sunflower2333)       |
| Samsung Galaxy S10 | samsung-beyond1qlte | ✅            | [Ww](https://github.com/Idonotkno)                      |
| Xiaomi 9           | xiaomi-cepheus      | ✅            | [qaz6750](https://github.com/qaz6750)                   |
| Xiaomi Hercules    | xiaomi-hercules     | ✅            | [Ww](https://github.com/Idonotkno)                      |
| Xiaomi K20 Pro     | xiaomi-raphael      | ✅            | [sunflower2333](https://github.com/sunflower2333)       |
| Xiaomi Mix3 5G     | xiaomi-andromeda    | ✅            | [sunflower2333](https://github.com/sunflower2333)       |
| Xiaomi Pad 5       | xiaomi-nabu         | ✅            | [Map220v](https://github.com/map220v)                   |
| Xiaomi Poco X3 Pro | xiaomi-vayu         | ❌            | [Mcusr120](https://github.com/mcusr120)                 |
| Meizu 16T          | meizu-m928q         | ❌            | NONE                                                    |
| Google Pixel 4 XL  | google-coral        |              | [Android-Piepaint](https://github.com/Android-Piepaint) |

### Acknowledgements

* [EFIDroid Project](http://efidroid.org/)
* Andrei Warkentin and his [RaspberryPiPkg](https://github.com/andreiw/RaspberryPiPkg)
* Sarah Purohit
* [Googulator](https://github.com/Googulator/)
* [Ben (Bingxing) Wang](https://github.com/imbushuo/)
* [Renegade Project](https://github.com/edk2-porting/)

### Copyright

©2022, The Renegade Project,All Rights Reserved.&#x20;

©2015\~2022,The GNU/Genshin Project,All Rights Reserved.

{% embed url="https://wiki.renegade-project.cn/en/home" %}

{% embed url="https://the-gnu-genshin-project.gitbook.io/gnu-genshin-project/" %}

### License

All code except drivers in `GPLDriver` directory are licensed under BSD 2-Clause. GPL Drivers are licensed under GPLv2 license.



\
\
