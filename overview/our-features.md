---
description: GNU/Genshin is a FreeBSD-Based operating system designed for embedded devices.
---

# ✨ Our Features

{% hint style="info" %}
**Note:** If you want to see more about our works, visit our official Wiki here ! [http://gnu-genshin.wikidot.com](http://gnu-genshin.wikidot.com)
{% endhint %}

## Welcome to GNU/Genshin !

GNU/Genshin is a FreeBSD-Based operating system designed for embedded devices.This guide provides a brief overview of some key knowledge points necessary to get the most out of using the GNU/Genshin operating system.

## Software install/update using GI(The default package manager of GNU/Genshin) <a href="#toc0" id="toc0"></a>

GI Package manager is the default package manager ships with GNU/Genshin operating system. GI is a simple but powerful tool to install software (applications ) from your system. Once user entered "gi" command in their terminal, GI will launch automatically and download specified applications from our official GNU/Genshin Software Repositories. Usually the application should be installed after finished downloading. There are 3 features of GI Package Manager:

1. No sudo or su required to execute.
2. Support install software or update your system from a specified repositories.
3. Install local packages is also supported.

## Kernel

GNU/Genshin uses a customized kernel based on FreeBSD 13.1 called "Genshin Kernel". The kernel is built with universal techniques to let GNU/Genshin to run on other platforms.

## Longer Support <a href="#toc12" id="toc12"></a>

GNU/Genshin usually releases major versions once a year, and minor versions are released in the form of updates. The normal version is supported for 3 months, the long-term support version is 10 years, but the Primogem Updates version is 50 years.
