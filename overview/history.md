---
description: History of The GNU/Genshin Project
---

# 🕰 History

In 2010, a people posted an article named " The Advantages of Using A BSD Kernel As An Operating System's Base".The article caught his attention, after reading some documents, he successfully created his first bootable images.

This actually is the first release of GNU/Genshin. However, it was never released, you can't find any report about this. Its version code seems ended as ".rc0". On July 28, 2015, The GNU/Genshin Project was formed.&#x20;

It is worth noting that a famous Minecraft Map called "Republic of FlashTeens", was also formed on that day. The project was initially called "The FTMC OS", is a Linux-Based operating system for x86 devices.&#x20;

Just like other Linux-Based embedded system softwares, it doesn't have its own package manager because it is designed only for subway managements.&#x20;

In late 2015, FTMC OS published its 2nd release:version 2.0 .Then,"The FTMC OS" was renamed as "The GNU/Genshin Project",and started the development of FreeBSD-based operating system.&#x20;

The FTMC OS however, continues to exist. In early 2017, FTMC OS merged with GNU/Genshin and added support for arm architecture.&#x20;

In 2021, a project dedicated to porting bootable UEFI Firmware to 64-bit arm devices, named "The Renegade Project" was formed. This caught his attention, again . He successfully booted GNU/Genshin 5.15 LTS on the OnePlus 6T, this means that GNU/Genshin officially enters the embedded devices.

&#x20;In 2022, another project called "Project-MU" launched, to help porting UEFI firmware to Qualcomm Snapdragon 855 devices, then the project creators successfully compiled the firmware for the Pixel 4 XL, and there is currently no progress.
