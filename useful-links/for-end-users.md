# 👨💻 For End Users

{% hint style="info" %}
**Good to know:** most questions can be found at our official Wiki !
{% endhint %}

Have questions? Don't panic! just look it up in our Wiki ! If you still don't know, ask our community for help is a good idea.

### English Wiki (Good for old browsers)

{% embed url="http://gnu-genshin.wikidot.com" %}

### Chinese Wiki (MediaWiki-Powered, require HTTPS in your browser) 

{% embed url="https://gnugenshin.miraheze.org/wiki/%E9%A6%96%E9%A0%81" %}

### The GNU/Genshin Project official website

{% hint style="info" %}
**Note:** If this site you are using cannot be visited or blocked from your network operator , consider visiting this site below.It has good compatiblity for old browsers.
{% endhint %}

{% embed url="https://gnu-genshin.blogspot.com" %}

{% embed url="http://gnu-genshin-project.wikidot.com/home:home" %}
