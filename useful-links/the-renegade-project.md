---
description: >-
  A project helps to port UEFI on your ARM-Based Phone, and make it possible to
  boot Windows® 11 or Linux.
---

# 📱 The Renegade Project

{% hint style="info" %}
Yes, It is possible to boot Windows® 11 or Linux OS on your phone.
{% endhint %}

{% hint style="warning" %}
As far as I know,only Qualcomm Snapdragon® 835,845(SDM845),\
850(SDM850) and 855(SM8150) devices are supported now.
{% endhint %}

Their project has Super-cow powers.

<figure><img src="https://wiki.renegade-project.cn/images/banner_dark.jpg" alt=""><figcaption><p>From their website. It's cool,isn't it ?</p></figcaption></figure>

{% embed url="https://wiki.renegade-project.cn/en/home" %}

## If you want to, go and check their support status now !

{% embed url="https://wiki.renegade-project.cn/en/devices" %}

Have questions in installing Windows® or buliding UEFI image for your device? \
You can check their Telegram groups, Discord Server and forums.

{% embed url="https://t.me/joinchat/MNjTmBqHIokjweeN0SpoyA" %}

{% embed url="https://discord.gg/XXBWfag" %}

{% embed url="https://forum.renegade-project.org/" %}
