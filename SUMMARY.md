# Table of contents

* [Welcome to The GNU/Genshin Project.](README.md)

## Overview

* [🕰 History](overview/history.md)
* [✨ Our Features](overview/our-features.md)

## Operating Systems

* [🌟 GNU/Genshin Unified](operating-systems/gnu-genshin-unified.md)
* [1 One Operating System](operating-systems/one-operating-system.md)

## About us

* [📪 About GNU/Genshin](about-us/about-gnu-genshin.md)
* [📎 Our splash screens, wallpapers\&logos ,etc.](about-us/our-splash-screens-wallpapers-and-logos-etc..md)

## Useful Links

* [👨💻 For End Users](useful-links/for-end-users.md)
* [📱 The Renegade Project](useful-links/the-renegade-project.md)
* [🚇 GNU/Genshin Subway Project](https://the-gnu-genshin-project.gitbook.io/gnu-genshin-subway-project/)

***

* [📶 GNU/Genshin UEFI Project for Pixel 4 XL(aka coral)](gnu-genshin-uefi-project-for-pixel-4-xl-aka-coral.md)
* [🖥 The FreeBSD Project](https://www.freebsd.org)
