---
description: >-
  GNU/Genshin Unified is an open-source, FreeBSD-based operating system that
  created in 2015 by FlashTeens Chiang.
---

# Welcome to The GNU/Genshin Project.

<figure><img src=".gitbook/assets/gnu-genshin-new-logo.png" alt=""><figcaption></figcaption></figure>

## Operating Systems

### GNU/Genshin Unified&#x20;

<figure><img src=".gitbook/assets/gnu-genshin-unified.png" alt=""><figcaption></figcaption></figure>

We, the GNU/Genshin Project, are developing a FreeBSD-Based operating system named GNU/Genshin Unified. To bring the universial experience to the end users, we also have a desktop operating system based on GNU/Genshin Unified, it's called One Operating System.

GNU/Genshin Unified is a FreeBSD-based, open-source operating system for everyone. We make it to just enough for your devices, support your device released more than 20 years ago, and it's free !

{% content-ref url="operating-systems/gnu-genshin-unified.md" %}
[gnu-genshin-unified.md](operating-systems/gnu-genshin-unified.md)
{% endcontent-ref %}

### One Operating System

One Operating System(OneOS) is a GNU/Genshin Unified distribution for desktop users.&#x20;

{% content-ref url="operating-systems/one-operating-system.md" %}
[one-operating-system.md](operating-systems/one-operating-system.md)
{% endcontent-ref %}

## Sub projects

### GNU/Genshin Subway Project

Hey, we also like playing Minecraft, the most popular 3D sandbox game. Everyone can build your magic kingdom and invite some friends to play!

The GNU/Genshin Subway Project (_Chinese:原信地鐵專案_) is a Minecraft 3rd party map project formed in 2019, it aims to bring a FT(FlashTeens Chiang, who created a Minecraft map _Republic of FlashTeens_ in 2015.)experience to every player.

<figure><img src=".gitbook/assets/2023-02-05_14.03.49.png" alt=""><figcaption><p>Wayland Shopping Center Station.</p></figcaption></figure>

Our development guide is available at GitBook:

{% embed url="https://the-gnu-genshin-project.gitbook.io/gnu-genshin-subway-project/" %}

Download our source code and our map in GitHub!

{% embed url="https://github.com/Android-Piepaint/GNU-Genshin-Subway-Project" %}

## We are the ONE.

&#x20;  Sometimes I feel like there's no hope, when I feel down, I think it's the end.&#x20;

&#x20;   I feel like I'm lonely, like I have no friend. Life ain't I always like what we planned.&#x20;

&#x20;    We've got to make builds and that's okay, sometimes we have to just let it run.&#x20;

&#x20;     'Cause it ain't always like what we wrote, it ain't always like what we wrote.&#x20;

&#x20;     We are the one, we are the one.

&#x20;      And open your eyes, see the worlds.

&#x20;        **We are the ONE.**
