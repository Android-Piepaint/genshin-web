# 📪 About GNU/Genshin

## GNU/Gen·shi·n ****&#x20;

_noun_

_(Chinese：原信；原信作業系統；原信操作系统)_

1. An **Free and Open-Suorced BSD-Based opreating system** designed for embedded devices.
2. A project aims to run Genshin Impact on Linux or Windows ARM.(The **GNU/Genshin** name actually was inspired from _**Genshin Impact**_)

### History

The GNU/Genshin Project was formed on July,28,2015, they released the first alpha version at the end of this month.

&#x20;Unlike their recent releases, their first release is Linux-based, and they use XFCE Desktop as its default graphical user interface.

There is a hidden file at the root directory, but nothing in there.

&#x20;A beta version was released in February 2016, at the same time it was announced that the project had entered its first stable milestone.

The first public beta version was released in April of the same year, in which a new FreeBSD-based kernel has been adopted, but since it does not come with a desktop environment by default, there are not many testers. But this release also lays the groundwork for future iterations.
