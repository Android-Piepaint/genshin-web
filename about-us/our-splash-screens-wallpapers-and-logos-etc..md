# 📎 Our splash screens, wallpapers\&logos ,etc.

## How we create these artworks

Just like other operating systems and software\&hardware companies have their own logos, we also need some artworks. Not only for user's customizations, but also to share our hard works to everyone, even if you don't know our projects.

<figure><img src="http://gnu-genshin.wikidot.com/local--files/start/splash-light-gruvbox.png" alt=""><figcaption><p>GNU/Genshin's default splash screen.</p></figcaption></figure>

You may think it is just some simple texts with some icons placed randomly, but actually we did this intentionally! From the first icons to the last, each icon have their own meaning.

* Wi-Fi icon: Means GNU/Genshin supports all compatible Wi-Fi cards.
* Bluetooth icon: Means GNU/Genshin supports Bluetooth devices.
* 64-Bit icon: GNU/Genshin is a 64-Bit OS for all devices that have 64-Bit architecture CPUs.
* Phone icon: GNU/Genshin also supports for your phone(we are BSD,not normal Android).
* Desktop PC icon: GNU/Genshin supports your x86\_64-Based PC.
* Laptop icon: GNU/Genshin supports all kinds of laptops, no matter CPU architecture is.

We use an open-source drawing software called **Inkscape** to create our artworks.

## Wallpapers

You can get our wallpapers by clicking the images below.



### Orginal

<figure><img src="../.gitbook/assets/wallpaper4k-nord.png" alt=""><figcaption><p>Orginal wallpaper in nord color.</p></figcaption></figure>

<figure><img src="../.gitbook/assets/wallpaper4k-gruvbox-dark.png" alt=""><figcaption><p>same as above. But it's gruvbox color.</p></figcaption></figure>

### Mountains

<figure><img src="../.gitbook/assets/genshin-wallpapers-nord-1.png" alt=""><figcaption><p>Snow Mountains, fits well with nord themes.</p></figcaption></figure>

#### Gruvbox themes

<figure><img src="../.gitbook/assets/gnu-genshin-gruvbox.png" alt=""><figcaption><p>4K Version.</p></figcaption></figure>

<figure><img src="../.gitbook/assets/mobile.png" alt=""><figcaption><p>Mobile version.</p></figcaption></figure>

## Logos

<figure><img src="../.gitbook/assets/gnu-genshin-logo-new-light-orginal.png" alt=""><figcaption></figcaption></figure>

<figure><img src="http://gnu-genshin.wikidot.com/local--files/start/gnu-genshin-logo-orginal.png" alt=""><figcaption><p>GNU/Genshin 's logo in 1024x1024.</p></figcaption></figure>

<figure><img src="http://gnu-genshin.wikidot.com/local--files/wiki:gallery/75ed116a7c2da97ab2ae3b8cbfaf9334.png" alt=""><figcaption><p>Original GNU/Genshin logo.</p></figcaption></figure>

<figure><img src="http://gnu-genshin.wikidot.com/local--files/wiki:gallery/gnu-genshin-logo-new.png" alt=""><figcaption><p>GNU/Genshin logo, inspired by Android 12 Themed icons.</p></figcaption></figure>

You can use and spread our logos at anywhere except for commercial usages.

And spell our name properly:

* GNU/Genshin :white\_check\_mark:
* The GNU/Genshin Project :white\_check\_mark:
* Project Genshin :white\_check\_mark:
* gnu/genshin:x:
* Genshin:x:
* gnugenshin:x:

Last, we hope you can enjoy our operating system and share it to everyone !
